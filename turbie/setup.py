# -*- coding: utf-8 -*-
"""Setup file for turbie
"""
from setuptools import setup


setup(name='turbie',
      version='0.0.1',
      description='Just a simple wind turbine',
      url='https://gitlab.windenergy.dtu.dk/python-at-risoe/codecamp',
      author='Jenni Rinker',
      author_email='rink@dtu.dk',
      license='MIT',
      packages=['turbie',  # top-level package
                ],
      install_requires=['matplotlib',  # plotting turbie results
                        'numpy',  # numeric arrays
                        'scipy',  # solve ivp
                        ],
      zip_safe=False)
