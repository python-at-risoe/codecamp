"""Simulate turbine's response to wind and save it.
"""
from pathlib import Path

import numpy as np
from scipy.integrate import solve_ivp

from turbie_helpers import dydt, calculate_ct, get_turbie_system_matrices, load_wind, save_turbie_res

wind_dir = Path(r'C:\Users\rink\Desktop\wind')
res_dir = Path(r'C:\Users\rink\Desktop\resp')

# load the system matrices
param_path = '../turbie_parameters.txt'
Dr, rho = np.loadtxt(param_path, comments='%')[-2:]
area = np.pi * (Dr / 2) **2  # rotor area
M, C, K = get_turbie_system_matrices()

# define our time vector
t0, tf, dt = 0, 660, 0.01
tspan = [t0, tf]  # 2-element list of start, stop
y0 = [0, 0, 0, 0]  # initial condition
t_eval = np.arange(t0, tf, dt)  # times at which we want output

# get generator of wind file paths
wind_paths = sorted(wind_dir.rglob('wind_*.txt'))

# loop over wind files
for i, wind_path in enumerate(wind_paths):
    print(f'{i+1}/{len(wind_paths)}')
    # define the resp path
    ti_dir = wind_path.parts[-2].replace('wind_', 'resp_')
    fname = wind_path.parts[-1].replace('wind_', 'resp_')
    resp_path = res_dir / ti_dir / fname
    # load the wind
    t_wind, wind = load_wind(wind_path)
    # calculate Ct from avg
    ct = calculate_ct(wind)
    # run solve_ivp
    args = (M, C, K, ct, rho, area, t_wind, wind)  # extra arguments to dydt besides t, y
    res = solve_ivp(dydt, tspan, y0, t_eval=t_eval, args=args)
    # save resp file
    save_turbie_res(res, resp_path, wind=(t_wind, wind))
