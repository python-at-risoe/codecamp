# -*- coding: utf-8 -*-
"""Helper functions for Turbie exercises
"""
from pathlib import Path

import numpy as np


def calculate_ct(u, ct_path='../CT.txt'):
    """Calculate Ct using lookup table and wind
    time series. ct = CT(U).
    """
    u_mean = np.mean(u)  # calculate mean of wind time series
    u_lut, ct_lut = np.loadtxt(ct_path, skiprows=1).T  # load lookup table
    ct = np.interp(u_mean, u_lut, ct_lut)  # interp Ct curve
    return ct


def calculate_psd(t, x, u=None):
    """Get the one-sided PSD of Turbie response (and forcing, if given)"""
    T = t[-1] + t[1]
    dt, df = T / t.size, 1 / T
    f = np.fft.rfftfreq(t.size, d=dt)
    S = np.abs(np.fft.rfft(x, axis=0))**2 / 2 / df
    if u is None:
        return f, S
    else:
        Su = np.abs(np.fft.rfft(u))**2 / 2 / df
        return f, S, Su


def dydt(t, y, M, C, K, ct, rho, area, t_wind, wind):
    """Differential function for Turbie (with forcing)"""
    Minv = np.linalg.inv(M)  # save inverse
    ndof = M.shape[0]  # get number of degrees of freedon
    # ---- assemble matrix A ----
    I, O = np.eye(ndof), np.zeros((ndof, ndof))
    A = np.block([[O, I], [-Minv @ K, -Minv @ C]])
    # ---- define forcing vector ----
    F = np.zeros(ndof)  # initialize forcing vector
    # calculate aerodynamic force
    x1dot = y[2]  # third element is blade velocity
    u = np.interp(t, t_wind, wind)  # interpolate current wind
    faero = 0.5 * rho * area * ct * (u - x1dot) * np.abs(u - x1dot)
    F[0] = faero
    # assemble array B
    B = np.zeros(2*ndof)  # initialize the array
    B[ndof:] = Minv @ F
    return A @ y + B


def get_turbie_system_matrices():
    """Assmble M, C and K for Turbie"""
    mb, mn, mh, mt, c1, c2, k1, k2, fb, ft, drb, drt, Dr, rho = load_parameters()
    m1 = 3*mb  # mass 1 is 3 blades
    m2 = mh + mn + mt  # mass 2 is hub, nacelle and tower
    M = np.array([[m1, 0], [0, m2]])  # mass matrix
    C = np.array([[c1, -c1], [-c1, c1+c2]])  # damping matrix
    K = np.array([[k1, -k1], [-k1, k1+k2]])  # stiffness matrix
    return M, C, K


def load_parameters(path='../turbie_parameters.txt'):
    """Load the parameters for Turbie from a text file"""
    path = '../turbie_parameters.txt'
    mb, mn, mh, mt, c1, c2, k1, k2, fb, ft, drb, drt, Dr, rho = \
        np.loadtxt(path, comments='%')
    return mb, mn, mh, mt, c1, c2, k1, k2, fb, ft, drb, drt, Dr, rho


def load_turbie_res(path):
    """"""
    res_arr = np.loadtxt(path, skiprows=1)
    t, u = res_arr[:, :2].T
    x = res_arr[:, 2:]
    return t, x, u


def load_wind(path):
    """Load the time and wind speed from a file.
    Returns t, u.
    """
    t_wind, wind = np.loadtxt(path, skiprows=1).T
    return t_wind, wind


def save_turbie_res(res, path, wind=None):
    """Save Turbie results to a text file at the specified path.
    Object res is the output of solve_ivp (global coordinates).
    If forced response, pass tp and u(tp) in as wind=(tp, up)."""
    path = Path(path)
    if wind is not None:
        tp, up = wind
    else:
        tp = res.t
        up = np.zeros(tp.size)
    # create parent folder if it doesn't exist
    path.parent.mkdir(parents=True) if not path.parent.is_dir() else None
    # calculate xb and xt from x1 and x2
    x1, x2 = res.y[:2, :]
    xt = x2
    xb = x1 - x2
    # make the save array and save it
    save_arr = np.empty((res.t.size, 4))
    save_arr[:, 0] = res.t
    save_arr[:, 1] = up
    save_arr[:, 2] = xb
    save_arr[:, 3] = xt
    np.savetxt(path, save_arr, fmt='%.3f', delimiter='\t', comments='',
               header='Time(s)	V(m/s)	xb(m)	xt(m)')
    return
