# -*- coding: utf-8 -*-
"""Load a simulation, calculate power spectrum and plot the time series and PSD
"""
import matplotlib.pyplot as plt
import numpy as np
from turbie_helpers import load_turbie_res, calculate_psd


# ----- load the results from file -----
# txtpath = '../results/free_response_1.0_0.2_0.0_0.0.txt'
txtpath = '../MATLAB_model/res_8_ms_TI_0.15.txt'
t, x, u = load_turbie_res(txtpath)


# ---- calculate psd -----
f, S, Su = calculate_psd(t, x, u)

# ----- check the variance -----
# var_u = np.var(u)
# sum_Su = np.trapz(Su, x=f)
# print(f'Wind variance: {var_u:.3f}')
# print('')


# ---- plot -----

fig, axs = plt.subplots(2, 1, num=1, clear=True, figsize=(10, 6))

ax = axs[0]
ax.plot(t, x[:, 0])
ax.plot(t, x[:, 1])
ax.set_ylabel('Free response [m]')
ax.set_xlabel('Time [s]')
ax.set_xlim([t[0], t[-1]])

ax = axs[1]
ax.semilogy(f, S[:, 0], label='Blade (x1)')
ax.semilogy(f, S[:, 1], label='Nacelle (x2)')
ax.set_ylabel('PSD [m^2/Hz]')
ax.set_xlabel('Frequency [Hz]')
ax.set_xlim([0, 4])

ylim = ax.get_ylim()
f1, f2 = 0.25, 0.63
ax.plot([f1, f1], ylim, '--', c='0.7', zorder=-2, label='Natural frequency')
ax.plot([f2, f2], ylim, '--', c='0.7', zorder=-2)
ax.set_ylim(ylim)

ax.legend()

fig.suptitle(f'Simulation: "{txtpath}"')

plt.tight_layout()
