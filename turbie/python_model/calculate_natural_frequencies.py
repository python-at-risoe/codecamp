# -*- coding: utf-8 -*-
"""Calcaulte damped and undamped natural frequencies and damping for Turbie
"""
import numpy as np
from scipy.linalg import eig
from turbie_helpers import get_turbie_system_matrices


M, C, K = get_turbie_system_matrices()


#%% Undamped eigenanalysis

# get eigenvalues and eigenvectors
lamda, v = eig(np.linalg.inv(M) @ K)

# sort by increasing lambda magnitude
sort_idx = np.argsort(np.abs(lamda))
v = v[:, sort_idx]
lamda = lamda[sort_idx]

# get natural frequencies from eigenvalues
wn = np.sqrt(np.abs(lamda))  # natural freq in rad/s
fn = wn / 2 / np.pi  # in Hz

print('\nUndamped eigenanalysis')
print('----------------------')
print('  fn   ' + '  '.join(f' {f:.3f}' for f in fn))
print('  v1   ' + '  '.join(f'{v[0,i]:6.3f}' for i in range(2)))
print('  v2   ' + '  '.join(f'{v[1,i]:6.3f}' for i in range(2)))


#%% Damped eigenanalysis

# assemble state-space matrix
I, O = np.eye(2), np.zeros((2, 2))
A = np.block([[O, I], [-np.linalg.inv(M) @ K, -np.linalg.inv(M) @ C]])

# get eigenvalues and eigenvectors
lamda, v = eig(A)

# take only unique values
lamda = lamda[::2]
v = v[:2, ::2]

# sort by increasing lambda magnitude
sort_idx = np.argsort(np.abs(lamda))
v = v[:, sort_idx]
lamda = lamda[sort_idx]

# calculate frequencies and damping
wd = np.imag(lamda)
fd = wd / 2 / np.pi

# calculate undamped natural frequencies and damping (percent critical)
wn = np.abs(lamda)
fn = wn / 2 / np.pi
zeta = -np.cos(np.angle(lamda))

print('\nDamped eigenanalysis')
print('----------------------------------------')
print('  fd  ' + '  '.join(f' {f:13.3f}' for f in fd))
print('  z   ' + '  '.join(f' {z*100:12.3f}%' for z in zeta))
print('  v1  ' + '  '.join(f'{v[0,i]:14.3f}' for i in range(2)))
print('  v2  ' + '  '.join(f'{v[1,i]:14.3f}' for i in range(2)))
