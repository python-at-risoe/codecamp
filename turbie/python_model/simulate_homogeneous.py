# -*- coding: utf-8 -*-
"""Simulate Turbie's response with no wind to initial conditions and save
"""
import numpy as np
from scipy.integrate import solve_ivp
from turbie_helpers import get_turbie_system_matrices, save_turbie_res


def dqdt_homogeneous(t, q, M, C, K):
    """Differential function for Turbie with no wind"""
    I, O = np.eye(2), np.zeros((2, 2))
    A = np.block([[O, I], [-np.linalg.inv(M) @ K, -np.linalg.inv(M) @ C]])
    return A @ q


# simulation settings
dt = 0.01  # time step
y0 = [1, 0.2, 0, 0]  # initial condition [x1, x2, v1, v2]
t_span = [0, 10]  # time to simulate

# ---- run simulation -----
M, C, K = get_turbie_system_matrices()
t_eval = np.arange(t_span[0], t_span[1], dt)
res = solve_ivp(dqdt_homogeneous, t_span, y0, args=(M, C, K), t_eval=t_eval)

# ----- save results -----
txtpath = '../results/free_response_' + '_'.join(f'{x:.1f}' for x in y0) + '.txt'
save_turbie_res(res, txtpath)
