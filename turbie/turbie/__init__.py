# -*- coding: utf-8 -*-
"""Functions for Turbie
"""
import matplotlib.pyplot as plt
import numpy as np


def calculate_ct(u, ct_path='CT.txt'):
    """Calculate Ct using lookup table and wind
    time series. ct = CT(U).
    """
    u_mean = np.mean(u)  # calculate mean of wind time series
    u_lut, ct_lut = np.loadtxt(ct_path, skiprows=1).T  # load lookup table
    ct = np.interp(u_mean, u_lut, ct_lut)  # interp Ct curve
    return ct


def dydt(t, y, M, C, K, ct, rho, area, t_wind, wind):
    """Differential function for Turbie (with forcing)"""
    Minv = np.linalg.inv(M)  # save inverse
    ndof = M.shape[0]  # get number of degrees of freedon
    # ---- assemble matrix A ----
    I, O = np.eye(ndof), np.zeros((ndof, ndof))
    A = np.block([[O, I], [-Minv @ K, -Minv @ C]])
    # ---- define forcing vector ----
    F = np.zeros(ndof)  # initialize forcing vector
    # calculate aerodynamic force
    x1dot = y[2]  # third element is blade velocity
    u = np.interp(t, t_wind, wind)  # interpolate current wind
    faero = 0.5 * rho * area * ct * (u - x1dot) * np.abs(u - x1dot)
    F[0] = faero
    # assemble array B
    B = np.zeros(2*ndof)  # initialize the array
    B[ndof:] = Minv @ F
    return A @ y + B


def get_turbie_system_matrices(turbie_path=None):
    """Assmble M, C and K for Turbie"""
    if turbie_path is None:
        turbie_path = 'turbie_parameters.txt'
    mb, mn, mh, mt, c1, c2, k1, k2, fb, ft, drb, drt, Dr, rho = np.loadtxt(turbie_path, comments='%')
    m1 = 3*mb  # mass 1 is 3 blades
    m2 = mh + mn + mt  # mass 2 is hub, nacelle and tower
    M = np.array([[m1, 0], [0, m2]])  # mass matrix
    C = np.array([[c1, -c1], [-c1, c1+c2]])  # damping matrix
    K = np.array([[k1, -k1], [-k1, k1+k2]])  # stiffness matrix
    return M, C, K


def load_results(path, t_start=0):
    """Load turbie results from text file. 
    Remove t_start from beginning.
    Returns 4 [nt] arrays: t, u, x1 and x2.
    """
    t, u, x1, x2 = np.loadtxt(path, skiprows=1, unpack=True)
    mask = t >= t_start
    return t[mask], u[mask], x1[mask], x2[mask]


def load_wind(path):
    """Load the time and wind speed from a file.
    Returns t, u.
    """
    t, u = np.loadtxt(path, skiprows=1).T
    return t, u


def plot_results(t, u, x1, x2, tspan=None):
    """Make a plot of the Turbie results array.
    Returns the figure and axes handles.
    """
    if tspan is None:  # if tspan not given, take whole range
        tspan = [t[0], t[-1]]
    
    # mask the data so it falls within tspan
    mask = (t >= tspan[0]) & (t <= tspan[1])
    t = t[mask]
    u = u[mask]
    x1 = x1[mask]
    x2 = x2[mask]
    
    fig, axs = plt.subplots(2, 1, clear=True, figsize=(10, 4))
    
    ax = axs[0]  # subplot 1: wind speed
    ax.plot(t,  u)
    ax.set_ylabel('Wind speed [m/s]')
    ax.set_xlim(tspan)
    
    ax = axs[1]  # subplot 2: blade and tower deflection
    ax.plot(t, x1, label='Blades')
    ax.plot(t, x2, label='Tower')
    ax.set_ylabel('Deflection [m]')
    ax.set_xlabel('Time [s]')
    ax.set_xlim(tspan)
    ax.legend()
    
    plt.tight_layout()  # make sure the axes fit the figure

    return fig, axs

