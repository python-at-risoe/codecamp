function dqdt = dqdt(t1,q1)

global t M C K rho Dr CT V

% Position and velocity
x1     = q1(1:2);
xdot1  = q1(3:4);

% Extract index
dt = mean(diff(t));
idx = round(t1/dt) + 1;

% Read wind speed
V1 = V(idx);

% Relative hub velocity
vrel1 = V1 - xdot1(1);

% Aerodynamic force
F1 = 0*x1;
F1(1) = 1/2*rho*pi/4*Dr^2*CT*vrel1*abs(vrel1);

% Populate output
dqdt = 0*q1;
dqdt(1:2) = xdot1;
dqdt(3:4) = M\(F1-C*xdot1-K*x1);

return