clc; clear; close all

%%

% Figure settings    
set(0,'DefaultAxesFontSize',14)
set(0,'DefaultTextFontSize',14)
set(0,'DefaultLineLineWidth',1.75)
set(0,'DefaultAxesXGrid','on','DefaultAxesYGrid','on')

global t f M C K rho Dr CT V

%% Turbine parameters

parameters = readvars('../turbie_parameters.txt', 'CommentStyle','%',  'NumHeaderLines',1, 'ExpectedNumVariables',14);
mb = parameters(1);         % Blade mass [kg]
mn = parameters(2);         % Nacelle mass [kg]
mh = parameters(3);         % Hub mass [kg]
mt = parameters(4);         % Tower mass [kg]

c1 = parameters(5);       % Blade flap structural damping [N/(m/s)]
c2 = parameters(6);       % Tower fore-aft structural damping [N/(m/s)]

k1 = parameters(7);       % Blade flap stiffness [N/m]
k2 = parameters(8);       % Tower fore-aft stiffness [N/m]

fb = parameters(9);          % First flap blade modal frequency [Hz]
ft = parameters(10);         % First fore-aft tower modal frequency [Hz]

drb = parameters(11);   % First flap blade modal damping ratio [-]
drt = parameters(12);   % First fore-aft tower modal damping ratio [-]

Dr = parameters(13);          % Rotor diameter (m)
rho = parameters(14);         % Air density (kg/m3)

%% System matrices

m1 = 3*mb;          % Combined 3-blade mass 
m2 = mh + mn + mt;  % Combined tower, nacelle and hub mass

% Mass matrix
M = [m1 0
    0   m2];

% Damping matrix
C = [c1 -c1
    -c1  c1+c2];

% Stiffness matrix
K = [k1 -k1
    -k1  k1+k2];

%% Undamped eigenanalysis

% Target natural frequencies
fntar = [fb ft]';

% Undamped natural frequencies
Du = eig(M\K);
fnu = sqrt(Du)/2/pi;

%% Damped eigenanalysis

% Target damping ratios
drtar = [drb drt]';

%State-space matrix
SS = [zeros(2) eye(2); -M\K -M\C];

% Damped natural frequencies and damping ratios
[~,Dd] = eig(SS);
[fnd,order] = sort(imag(diag(Dd))/2/pi,'descend');
dr = -real(diag(Dd))./abs(diag(Dd));
dr = dr(order);
dr = dr(fnd>0);
fnd = fnd(fnd>0);

% Calculate error and compare in table
fnerr = round((fnd./fntar-1)*100,1);
drerr = round((dr./drtar-1)*100,1);
table(fntar,fnu,fnd,fnerr,drtar,dr,drerr)

%% Thrust coefficient

% Load table with CT as function of mean wind speed
temp = importdata('../CT.txt');
Vtab = temp.data(:,1);
CTtab = temp.data(:,2);

figure()
plot(Vtab,CTtab,'ko'), hold on, grid minor, axis tight
xlabel(temp.colheaders{1})
ylabel(temp.colheaders{2})

%% Simulation parameters

% Time step (s)
dt = 0.01;

% Transient time (s)
Ttrans = 60;

% Cut-off frequency (Hz)
fcut = 1;

%% Environmental conditions

% Turbulence length scale (m)
l = 340.2;

% List of durations (s), mean wind speeds (m/s) and TI values
Tdurvec = [660  660     660];
V10vec  = [8    12      20];
TIvec   = [0.15 0.10    0.08];

for kk = 1:length(V10vec)
    
    % Environmental conditions for this case
    Tdur = Tdurvec(kk);
    V10 = V10vec(kk);
    TI = TIvec(kk);
    
    % Time vector
    t = 0:dt:Tdur-dt;
    
    % frequency vector
    df = 1/Tdur;
    f = df:df:fcut;
    
    % Time series of wind speed
    V = turbulent(V10,TI,l,1);
    
    % Thrust coefficient
    CT = pchip(Vtab,CTtab,V10);
    figure(1)
    plot(V10,CT,'rx')
    
    % Solve the response and assign channels
    q = ode4(@dqdt,t,[0;0;0;0]);
    xb = q(:,1)'-q(:,2)';
    xt = q(:,2)';
    
    figure()
    
    subplot(3,2,1)
    plot(t,V), grid minor, axis tight
    ylabel('V (m/s)')
    
    subplot(3,2,2)
    [freq,~,spec] = freqSpectrum(t,V,0);
    plot(freq,spec), grid minor, axis tight
    xlim([0 fcut])
    ylabel('PSD ((m/s)^2/Hz)')
    
    subplot(3,2,3)
    plot(t,xb), grid minor, axis tight
    ylabel('xb (m)')
    
    subplot(3,2,4)
    [freq,~,spec] = freqSpectrum(t(t>Ttrans),xb(t>Ttrans),0);
    plot(freq,spec), hold on, grid minor, axis tight
    plot([fnd(1) fnd(1)],[0 max(spec)],'k--')
    xlim([0 fcut])
    ylabel('PSD ((m)^2/Hz)')    
    
    subplot(3,2,5)
    plot(t,xt), grid minor, axis tight
    ylabel('xt (m)')
    xlabel('Time (s)')
    
    subplot(3,2,6)
    [freq,~,spec] = freqSpectrum(t(t>Ttrans),xt(t>Ttrans),0);
    plot(freq,spec), hold on, grid minor, axis tight
    plot([fnd(2) fnd(2)],[0 max(spec)],'k--')
    xlim([0 fcut])
    ylabel('PSD ((m)^2/Hz)')     
    xlabel('Frequency (Hz)')
    
    % Write to file
    filename = strcat('res_',num2str(V10),'_ms_TI_',num2str(TI),'.txt');
    fileID = fopen(filename,'w');
    fprintf(fileID,'Time(s)\tV(m/s)\txb(m)\txt(m)\n');
    for it = 1:length(t)
        fprintf(fileID,'%.3f\t%.3f\t%.3f\t%.3f\n',t(it),V(it),xb(it),xt(it));
    end
    fclose(fileID);
    
end
