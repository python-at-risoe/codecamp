import random

ngroups = 8
groups = [x+1 for x in range(ngroups)]

nrooms = ngroups // 2  # assuming even...

random.shuffle(groups)

print('\nROUND 1\n')
for iroom in range(nrooms):
    print('Group', groups[iroom*2], 'and Group', groups[iroom*2 + 1])


# change partner B in every room
groups[1::2] = random.sample(groups[1::2], nrooms)

print('\nROUND 2\n')
for iroom in range(nrooms):
    print('Group', groups[iroom*2], 'and Group', groups[iroom*2 + 1])
