.. CodeCamp


DTU Wind Energy CodeCamp
===========================================

        *Spend less time coding...and more time learning!*

Welcome to CodeCamp, a week-long, fun workshop dedicated to making you 
a better programmer!

  **<CodeCamp August 16-19 2022!!! Check announcements on Inside to register.>**

As engineers, we often spend much of our time in education learning
about math, science and physics. Times, however, are changing, and
more and more of the tasks we need to complete utilize programming
skills to simulate, post-process, and visualize data.

This workshop is aimed towards new master's students who intend to
take a majority of their courses from the Wind Energy department. 
(Returning students are also welcome, pending space constraints.) Some
coding experience is expected, but you by no means need to be an expert!
If you still have questions, please read more in the
:ref:`what_is_codecamp` and :ref:`faq` sections.

**If you missed CodeCamp**. You can still complete the exercises!
The "Turbie" section has everything you will need to brush
up your skills. Happy coding. :)

Contents:
    .. toctree::
        :maxdepth: 2
    
        what_is_codecamp
        faq
        schedule
        material
        preparation
        turbie/index