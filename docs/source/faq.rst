.. _faq:

Frequently Asked Questions
============================


**Who is teaching?**  
    `Jenni <https://www.dtu.dk/english/service/phonebook/person?id=118756&tab=2&qt=dtupublicationquery>`_
    and `Antonio <https://www.dtu.dk/english/Service/Phonebook/Person?id=85147&cpid=&tab=2>`_.
    Jenni is the course responsible for the LAC course and codes in
    Python. Antonio is an instructor in the Offshore Wind Energy
    course and codes in Matlab.

**Who can attend?**
    Priority is given to new students to DTU Wind Energy, but we
    also welcome returning students and alumni pending space 
    constraints.

**What programming language do you teach?**  
    Typically the exercises in CodeCamp are code-agnostic, meaning
    that you can complete them with Python, Matlab, Julia, R,
    whatever! Special editions of CodeCamp may focus on a specific
    language, but we generally recommend that you use Python or
    Matlab, as those are the languages that Antonio/Jenni are most
    familiar with.

**What COVID-19 measures will be taken during the camp?**  
    This is a difficult question to answer due to how quickly
    the COVID-19 situation changes. We will take all necessary
    measures to ensure everyone's health and safety, and perhaps some
    extra measures if we feel they are needed. More details will be
    provided as we get closer to CodeCamp.
   
**What level of programming skills should I have?**  
    Please see :ref:`prep`.
    
**Can I get ECTS for the workshop?**  
    It is possible to combine this workshop with extra exercises
    for a 5-ECTS special course. Please see :ref:`5ects` for more
    details.
    
**Can I attend remotely?**  
    Yes. We will offer a remote-attendance option for those who are 
    unable to attend in person. We will record the lectures in case
    of a poor internet connection, and we will pair up online
    attendants for the programming exercises to try to reduce Zoom
    fatigue.
    
**Is there an exam?**  
    This is not a course, so there is no exam! There will be a small
    final project, which gives you the chance to get feedback
    on your code, but it will not be graded.
    
**How do I register?**  
    Please see the announcement about CodeCamp on Inside.
    
**Is there a cap on attendance?**  
    Yes, due to teaching limitations. We will therefore prioritize
    new students, followed by returning students, and lastly alumni
    if there is space.
    
**What do I do if my question is not answered here?**  
    Shame our lack of foresight by sending an email to Jenni 
    (rink AT dtu DOT dk) with your question.
