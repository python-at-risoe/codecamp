.. _what_is_codecamp:

What is CodeCamp?
===========================

Here are all the juicy details you want to know about CodeCamp.
If you don't see the answer to your question here or in the 
:ref:`faq` section, please email Jenni (rink AT dtu DOT dk).


Who, when, where
-----------------

CodeCamp is intended for new masters students to get them ready to
go for their education at DTU Wind Energy.

The 2022 CodeCamp will happen August 16-19 (Tu-Fr) at DTU Lyngby campus. We will
offer the option to attend remotely to work with travel plans, but
sitting in front of a computer for 8 hours a day for several days straight
is suboptimal. Therefore we strongly encourage you to make plans to
attend physically, if possible.

The mad minds behind the camp are two instructors at DTU Wind Energy: `Jenni <https://www.dtu.dk/english/service/phonebook/person?id=118756&tab=2&qt=dtupublicationquery>`_ and
`Antonio <https://www.dtu.dk/english/Service/Phonebook/Person?id=85147&cpid=&tab=2>`_.
Jenni is the course responsible for the LAC course (`46320 <https://kurser.dtu.dk/course/2021-2022/46320>`_),
and Antonio is an instructor in Offshore Wind Energy (`46211 <https://kurser.dtu.dk/course/2021-2022/46211>`_).
Antonio is a Matlab user while Jenni is a Python user, but
we are both familiar with the other language. Since we have
experience teaching in the courses and have seen how difficult it
can sometimes be for students to complete the coding necessary for
assignments, we are designing this workshop to help get everyone up
to speed.


Camp philosophy
----------------

We have designed this camp with two main principles: (1) Give
students a chance to practice needed coding skills, and (2) Have fun
while doing it. We do expect that students come in with some basic
knowledge of coding (see :ref:`faq`), but we don't expect students to
be experts. The exercises we teach will be code-agnostic, meaning that
we don't mind if you use Matlab, Python, or some other language to
complete them, and Antonio and Jenni will be ready to support your
learning throughout the week.

The other important aspect of CodeCamp is collaboration. You will be
working in groups in many classes here at DTU Wind Energy, so we
will give several opportunities to work in pairs or small groups.

.. _5ects:

5-ECTS special course
-----------------------------

CodeCamp alone does not come with any ECTS. However, there is an
option to extend the camp with extra topics and exercises so that it
qualifies as a 5-ECTS course. The extra efforts correspond to
approximately 80 hours of work outside of the 40-hour camp. Extra 
topics are flexibile based on student needs, but possible options are
git/GitLab, Anaconda environments, making a Python package, and
tutorials of additional Python packages.


.. _prep:

Pre-requisites/prep material
-----------------------------

Before CodeCamp begins, we will expect that you (1) have installed
Anaconda/Spyder and/or Matlab and (2) have finished the basic
preparatory exercises that will be sent out in late July. We 
expect you to be familiar with basic programming concepts like
if/else blocks, for/while loops, etc. More details are 
provided in the Preparation for Day 1 tab.


Previous editions
------------------

CodeCamp has been held before! Here is the list of previous
editions:  

 * January 24-28, 2022. Python.  
 * August 16-20, 2021. Matlab/Python.
