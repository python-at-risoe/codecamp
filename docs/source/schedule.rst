.. _schedule:

Schedule
==========

CodeCamp will start every day at **9 am sharp**. For those attending
physically, we will see you on Lyngby campus (room TBD).
For those of you attending remotely, a Zoom link will be sent
out via email. Be sure to give yourself enough time in the morning to
get your laptops set up!

We aim for the following daily schedule:  
  * 9am: CodeCamp begins
  * 12pm: Break for lunch
  * 1pm: CodeCamp resumes
  * 4pm(?): CodeCamp ends

The end time is not fixed because the afternoon is primarily reserved
for coding exercises. On Wednesday, you have the option to attend a
social event, which will last through dinner time.

Here is the schedule of topics for CodeCamp 2022 (subject to change).

.. image:: _static/schedule.png
  :width: 700
  :alt: CodeCamp schedule
  

