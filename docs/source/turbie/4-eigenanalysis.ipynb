{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fa60cf2d",
   "metadata": {},
   "source": [
    "# Eigenanalysis\n",
    "\n",
    "When analysing a dynamical system, it is always beneficial to know the natural frequencies of the system. These are frequencies at which the system \"likes\" to vibrate, and an oscillatory force near this frequency will have result in a larger system response than at other frequencies.\n",
    "\n",
    "For a dynamical system with a set of mass, stiffness, and damping matrices, the undamped natural frequencies are calculated from the following generalized eigenvalue problem:  \n",
    "\\begin{equation}\n",
    "    \\lambda[M]\\boldsymbol{v} = [K]\\boldsymbol{v}.\n",
    "\\end{equation}\n",
    "If $[M]$ is invertible, this reduces to the standard eigenvalue problem,  \n",
    "\\begin{equation}\n",
    "    \\lambda\\boldsymbol{v} = [A]\\boldsymbol{v},\n",
    "\\end{equation}\n",
    "where $[A]=[M]^{-1}[K]$. The eigenvalue problem can be solved using the `eig` (Matlab) and `np.linalg.eig` (Python) functions. The natural frequency in radians/sec is directly related to the eigenvalue $\\lambda$:  \n",
    "\\begin{equation}\n",
    "    \\lambda = \\omega^2.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01046bff",
   "metadata": {},
   "source": [
    "### Exercises for the reader\n",
    "\n",
    "  1. Create a function that returns Turbie's mass, stiffness and damping matrices. It is okay to hard-code Turbie's parameters (available in the Definition tab) in the function, or you can try to load them from `turbie_parameters.txt`.  \n",
    "  2. Create a function that takes the mass and stiffness matrices as input and returns an array of the undamped natural frequencies **in Hz**. Be careful with units!  \n",
    "  3. **Optional**. Reexamine the PSD plot you made considering the natural frequencies you calculated. Do you see anything of interest?\n",
    "\n",
    "### Answers to the exercises\n",
    "\n",
    "#### Exercise 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "ff463f58",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Mass matrix: \n",
      " [[ 123000.       0.]\n",
      " [      0. 1179000.]]\n",
      "Damping matrix: \n",
      " [[ 4208. -4208.]\n",
      " [-4208. 16938.]]\n",
      "Stiffness matrix: \n",
      " [[ 1711000. -1711000.]\n",
      " [-1711000.  4989000.]]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "def get_turbie_system_matrices():\n",
    "    \"\"\"Assmble M, C and K for Turbie\"\"\"\n",
    "    turbie_path = 'turbie_parameters.txt'\n",
    "    mb, mn, mh, mt, c1, c2, k1, k2, fb, ft, drb, drt, Dr, rho = np.loadtxt(turbie_path, comments='%')\n",
    "    m1 = 3*mb  # mass 1 is 3 blades\n",
    "    m2 = mh + mn + mt  # mass 2 is hub, nacelle and tower\n",
    "    M = np.array([[m1, 0], [0, m2]])  # mass matrix\n",
    "    C = np.array([[c1, -c1], [-c1, c1+c2]])  # damping matrix\n",
    "    K = np.array([[k1, -k1], [-k1, k1+k2]])  # stiffness matrix\n",
    "    return M, C, K\n",
    "\n",
    "M, C, K = get_turbie_system_matrices()\n",
    "\n",
    "print('Mass matrix: \\n', M)\n",
    "print('Damping matrix: \\n', C)\n",
    "print('Stiffness matrix: \\n', K)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d4d9bc5",
   "metadata": {},
   "source": [
    "#### Exercise 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "7e11efc0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Undamped natural frequencies (Hz):  [0.25000017 0.63011524]\n"
     ]
    }
   ],
   "source": [
    "def get_undamped_natural_frequencies(M, K, in_hz=True):\n",
    "    \"\"\"Calculate the undamped natural frequencies of a system in Hz.\n",
    "    Arguments: mass, stiffness and damping matrices.\n",
    "    \"\"\"\n",
    "    lamda = np.linalg.eig(np.linalg.inv(M) @ K)[0]  # solve eiegenvalue problem (only evals)\n",
    "    omega = np.sort(np.sqrt(lamda))  # nat freqs in rad/s (ascending order)\n",
    "    freqs = omega / 2 / np.pi  # in Hz\n",
    "    if in_hz:\n",
    "        return freqs\n",
    "    else:\n",
    "        return omega\n",
    "\n",
    "freqs = get_undamped_natural_frequencies(M, K)\n",
    "print('Undamped natural frequencies (Hz): ', freqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e7f244a",
   "metadata": {},
   "source": [
    "#### Exercise 3\n",
    "\n",
    "Below is a reprint of the PSD plot from the PSD exercise. We can see peaks at 0.25 Hz and at 0.63 Hz, which matches our natural frequencies! This is a nice demonstration of how the energy in the input signal, given by the blue line, is redistributed to new frequencies in the response.\n",
    "\n",
    "<img src=\"figures/3-psd.png\" alt=\"Power spectral density\" style=\"width: 600px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d19df525",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
