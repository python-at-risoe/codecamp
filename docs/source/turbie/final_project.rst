.. _final_project:

Final project
============================

Download the wind files you will need for the final
project:  
 * :download:`Download 10% TI wind <simulations/wind_TI_0.1.zip>` 

Your task is to create two sets of plots: (1) the mean of the
blade and tower deflections versus mean wind speed and (2)
the standard deviation of the blade and tower deflections
versus mean wind speed. To do this, you will need to run
Turbie on all of the provided wind files and save the results.
You should then write code that loads the results files,
calculates the statistics you need, and plots the results.

**Optional expansion!** The files above only have wind with 10%
TI. To dig further, also analyse the 5% and 15% TI wind 
files provided below. Each of your plots will then have 
3 lines, one corresponding to each TI.


Optional extra files:  
 * :download:`Download 5% TI wind <simulations/wind_TI_0.05.zip>`  
 * :download:`Download 15% TI wind <simulations/wind_TI_0.15.zip>`  

Results files for testing/verification:  
 * :download:`Download 10% TI results <simulations/resp_TI_0.1.zip>`  
 * :download:`Download 5% TI results <simulations/resp_TI_0.05.zip>`  
 * :download:`Download 15% TI results <simulations/resp_TI_0.15.zip>`  