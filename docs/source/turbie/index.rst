.. turbie


Turbie
=========

We would like to introduce you to your new best
friend: Turbie. Turbie is a simple model of
a wind turbine, with just two degrees of freedom,
but we will use her in CodeCamp to buff up our
coding skills via eigenanalyses, PSD plots,
and analyses of time-marching responses.


Contents:
    .. toctree::
        :maxdepth: 2
    
        1-definition
        2-loading-plotting
        3-psd
        4-eigenanalysis
        5-homogeneous-response
        6-forced-response
        7-all_together
        final_project