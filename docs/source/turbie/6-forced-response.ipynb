{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fa60cf2d",
   "metadata": {},
   "source": [
    "# Forced response and save to file"
   ]
  },
  {
   "cell_type": "raw",
   "id": "7b072291",
   "metadata": {
    "raw_mimetype": "text/restructuredtext"
   },
   "source": [
    "To do the exercises in this notebook, please download the wind file and save it as ``wind_12_ms_TI_0.1.txt``: :download:`download file <wind_12_ms_TI_0.1.txt>`. You will also need the results file from the loading and plotting exercises."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd77dbf1",
   "metadata": {},
   "source": [
    "It is now time to add the wind to our Turbie simulations. We modify our dynamical system so it now has forcing:\n",
    "\\begin{equation}\n",
    "[M]\\ddot{\\overline{x}}(t) + [C]\\dot{\\overline{x}}(t) + [K]\\overline{x}(t) = \\overline{F}(t).\n",
    "\\end{equation}\n",
    "With our state vector equal to $\\overline{y}(t)=[\\overline{x}(t),\\dot{\\overline{x}}(t)]^T$, just as before, the state-space form is\n",
    "\\begin{equation}\n",
    "    \\overline{y}'(t) = \\left[ \\begin{array}{cc}   [0]_{N\\times N} & [I]_{N\\times N} \\\\\n",
    "                         -[M]^{-1}[K] & -[M]^{-1}[C] \n",
    "        \\end{array}\\right]\n",
    "        \\overline{y}(t) + \\left[ \\begin{array}{cc}   0  \\\\ [M]^{-1}\\overline{F}(t)  \n",
    "        \\end{array}\\right] \\\\\n",
    "\\end{equation}\n",
    "where $N$ is the number of degrees of freedom. For Turbie, $N=2$. We can simplify this expression as  \n",
    "\\begin{equation}\n",
    "    \\overline{y}'(t) = [A]\\overline{y}(t) + [B(t)]\n",
    "\\end{equation}\n",
    "\n",
    "Remember from the definition of Turbie that  \n",
    "\\begin{equation}\n",
    "    f_{aero}(t) = \\frac12\\, \\rho\\, C_T\\, A\\, [u(t) - \\dot{x}_1(t)]\\,|u(t) - \\dot{x}_1|,\n",
    "\\end{equation}\n",
    "where $C_T$ is constant for a given simulation and calculated from a look-up table using the mean wind speed of a simulation, $U=\\overline{u(t)}$. The aerodynamic forcing occurs only on the blades, so the forcing vector for our 2-DOF system is  \n",
    "\\begin{equation}\n",
    "\\overline{F}(t) = \\left[ \\begin{array}{c} f_{aero}(t)  \\\\ 0\\end{array} \\right].\n",
    "\\end{equation}\n",
    "\n",
    "**IMPORTANT!!!*** The output of your `dydt` function must be a 1-dimenstional array of length 4. In other words, if you test your `dydt` function and look at the shape of the output, you should get a shape of `(4,)`. **DO NOT** initialize your force vector as a 4x1 array, or your output will be the wrong size due to something called \"broadcasting\".\n",
    "\n",
    "### Exercises for the reader\n",
    "\n",
    "1. Create a function that takes as input the path to a wind file and returns the time array and the wind array. Try it on the wind file. What is the wind speed at $t=1$ s?  \n",
    "2. Create a function that takes as input a wind array and returns the value for $C_T$ to be used in a simulation. Test it on the wind file.  \n",
    "3. Explain how we can get the value of $\\dot{x}_1(t)$ from vector $y(t)$?\n",
    "4. Update your `dydt` function from the homogeneous response to include the aerodynamic forcing as defined above. What is the value of your new `dydt` function when $t=1$ and $y=[1, 2, 3, 4]$?  What is the shape of the output?\n",
    "5. Use your numerical integrator, just like in the homogeneous response, to simulate Turbie's response with the following conditions.   \n",
    "   * Time vector spans from 0 to 660 in spaces of 0.01 s.  \n",
    "   * Turbie initial conditions are `y = [0, 0, 0, ]`.  \n",
    "   * Use the wind time series in the response file you used for the \"Loading and Plotting\" and \"PSD\" exercises.\n",
    "6. Compare the tower and blade deflection in the results file versus the deflections you just simulated. Do they match?  \n",
    "  * **Remember** the blade deflection in the file is *relative* deflection with respect to the tower!!!\n",
    "7. Save the results to a text file in a format similar to the results file we give you.  \n",
    "  * **Python hint**: Use `np.savetxt` with `delimiter='\\t'` and `fmt='%.3f'` to get a nice file. You should also pass in a header string that explains the columns.\n",
    "\n",
    "### Answers to the exercises\n",
    "\n",
    "#### Exercise 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "305fdb38",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "def load_wind(path):\n",
    "    \"\"\"Load the time and wind speed from a file.\n",
    "    Returns t, u.\n",
    "    \"\"\"\n",
    "    t_wind, wind = np.loadtxt(path, skiprows=1).T\n",
    "    return t_wind, wind\n",
    "\n",
    "# test the function\n",
    "path = 'wind_12_ms_TI_0.1.txt'\n",
    "t_test = 1  # time we want the wind speed\n",
    "t_wind, wind = load_wind(path)  # load the wind\n",
    "idx_test = np.argmin(np.abs(t_wind - t_test))  # index closest to t_test\n",
    "print(wind[idx_test])  # wind speed at that index"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "047090f9",
   "metadata": {},
   "source": [
    "#### Exercise 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "131be2c3",
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_ct(u, ct_path='CT.txt'):\n",
    "    \"\"\"Calculate Ct using lookup table and wind\n",
    "    time series. ct = CT(U).\n",
    "    \"\"\"\n",
    "    u_mean = np.mean(u)  # calculate mean of wind time series\n",
    "    u_lut, ct_lut = np.loadtxt(ct_path, skiprows=1).T  # load lookup table\n",
    "    ct = np.interp(u_mean, u_lut, ct_lut)  # interp Ct curve\n",
    "    return ct\n",
    "\n",
    "print('The Ct for this wind series is: ', calculate_ct(wind))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c826bae",
   "metadata": {},
   "source": [
    "#### Exercise 3\n",
    "\n",
    "Recall that the state vector is equal to the displacements concatenated with the velocities. Therefore, $\\dot{x}_1(t)$ is the third element of $y(t)$ for a 2DOF system.\n",
    "\n",
    "#### Exercise 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7fd4f63",
   "metadata": {},
   "outputs": [],
   "source": [
    "def dydt(t, y, M, C, K, ct, rho, area, t_wind, wind):\n",
    "    \"\"\"Differential function for Turbie (with forcing)\"\"\"\n",
    "    Minv = np.linalg.inv(M)  # save inverse\n",
    "    ndof = M.shape[0]  # get number of degrees of freedon\n",
    "    # ---- assemble matrix A ----\n",
    "    I, O = np.eye(ndof), np.zeros((ndof, ndof))\n",
    "    A = np.block([[O, I], [-Minv @ K, -Minv @ C]])\n",
    "    # ---- define forcing vector ----\n",
    "    F = np.zeros(ndof)  # initialize forcing vector\n",
    "    # calculate aerodynamic force\n",
    "    x1dot = y[2]  # third element is blade velocity\n",
    "    u = np.interp(t, t_wind, wind)  # interpolate current wind\n",
    "    faero = 0.5 * rho * area * ct * (u - x1dot) * np.abs(u - x1dot)\n",
    "    F[0] = faero\n",
    "    # assemble array B\n",
    "    B = np.zeros(2*ndof)  # initialize the array\n",
    "    B[ndof:] = Minv @ F\n",
    "    return A @ y + B\n",
    "\n",
    "from turbie import get_turbie_system_matrices\n",
    "\n",
    "# load/calculate necessary parameters\n",
    "Dr, rho = np.loadtxt('turbie_parameters.txt', comments='%')[-2:]\n",
    "area = np.pi * (Dr / 2) **2  # rotor area\n",
    "M, C, K = get_turbie_system_matrices()\n",
    "ct = calculate_ct(wind)\n",
    "\n",
    "# test the function\n",
    "t_test, y_test = 1, [1, 2, 3, 4]\n",
    "dydt(t_test, y_test, M, C, K, ct, rho, area, t_wind, wind)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1af79848",
   "metadata": {},
   "source": [
    "#### Exercise 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d118383",
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.integrate import solve_ivp\n",
    "\n",
    "# define our time vector\n",
    "t0, tf, dt = 0, 660, 0.01\n",
    "\n",
    "# inputs to solve ivp\n",
    "tspan = [t0, tf]  # 2-element list of start, stop\n",
    "y0 = [0, 0, 0, 0]  # initial condition\n",
    "t_eval = np.arange(t0, tf, dt)  # times at which we want output\n",
    "args = (M, C, K, ct, rho, area, t_wind, wind)  # extra arguments to dydt besides t, y\n",
    "\n",
    "# run the numerical solver\n",
    "res = solve_ivp(dydt, tspan, y0, t_eval=t_eval, args=args)\n",
    "\n",
    "# extract the output\n",
    "t, y = res.t, res.y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2cd5b73e",
   "metadata": {},
   "source": [
    "#### Exercise 6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aeb17bc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "from turbie import load_results\n",
    "\n",
    "# load values from given results file\n",
    "path = 'resp_12_ms_TI_0.1.txt'\n",
    "t_wind, _, xb_txt, xt_txt = load_results(path)  # load results file (skip wind)\n",
    "\n",
    "# get out relative deflections\n",
    "xb_sim = y[0, :] - y[1, :]  # relative blade deflection\n",
    "xt_sim = y[1, :]  # tower deflection\n",
    "\n",
    "# plot them\n",
    "fig, axs = plt.subplots(2, 1, figsize=(9, 4))\n",
    "axs[0].plot(t_wind, xb_txt, label='In file')\n",
    "axs[0].plot(t, xb_sim, label='Simulated')\n",
    "axs[0].legend()\n",
    "axs[0].set_xlim([100, 200])\n",
    "axs[1].plot(t_wind, xt_txt, label='In file')\n",
    "axs[1].plot(t, xt_sim, label='Simulated')\n",
    "axs[1].set_xlim([100, 200]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "756cf057",
   "metadata": {},
   "source": [
    "They match, yay!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c70b6f82",
   "metadata": {},
   "source": [
    "#### Exercise 7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1144c1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# create the 2D array we want to save\n",
    "nt = t_eval.size  # number of time steps\n",
    "ncol = 4  # time, wind speed, xb, xt\n",
    "out_arr = np.empty((nt, ncol))\n",
    "out_arr[:, 0] = t_eval\n",
    "out_arr[:, 1] = np.interp(t_eval, t_wind, wind)\n",
    "out_arr[:, 2] = xb_sim\n",
    "out_arr[:, 3] = xt_sim\n",
    "\n",
    "# save the file\n",
    "out_path = 'myres.txt'\n",
    "header = 'Time(s)\\tV(m/s)\\txb(m)\\txt(m)'\n",
    "np.savetxt(out_path, out_arr, header=header, fmt='%.3f', delimiter='\\t', comments='')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d75df559",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
