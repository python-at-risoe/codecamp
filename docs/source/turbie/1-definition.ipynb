{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7aa1a63b",
   "metadata": {},
   "source": [
    "# Definition\n",
    "\n",
    "Turbie is a simple, two-degree-of-freedom (2DOF) system based of the DTU 10 MW Reference Wind Turbine. She is equivalent to the forced 2DOF mass-spring-damper system shown below, which is defined by a mass matrix, stiffness matrix, and damping matrix.\n",
    "\n",
    "<img src=\"figures/1-diagram.png\" alt=\"Diagram of Turbie\" style=\"width: 600px;\"/>"
   ]
  },
  {
   "cell_type": "raw",
   "id": "6aa637f1",
   "metadata": {
    "raw_mimetype": "text/restructuredtext"
   },
   "source": [
    "Here are two files that you can download with parameters for Turbie: :download:`download turbie_parameters.txt <turbie_parameters.txt>` and :download:`download CT.txt <CT.txt>`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da4474f2",
   "metadata": {},
   "source": [
    "## Mass, stiffness and damping matrices\n",
    "\n",
    "To derive Turbie's mass, stiffness and damping matrices, we make a few assumptions about our dynamical system:  \n",
    "* The turbine can only move in the fore-aft direction;  \n",
    "* The 3 blade deflections in the fore-aft direction are syncronized, i.e., only collective flapwise deflections.  \n",
    "\n",
    "With these assumptions, we have reduced Turbie to two degrees of freedom: the deflection of the blades in the fore-aft direction and the deflection of the nacelle in the fore-aft direction. The 2 DOFs of the system are therefore defined as  \n",
    "* $x_1(t)$: the deflection of the blades from their undeflected position in the global coordinate system;  \n",
    "* $x_2(t)$: the deflection of the nacelle from its undeflected position in the global coordinate system.\n",
    "\n",
    "With these DOFs, Turbie is equivalent to a 2DOF mass-spring-damper system, as shown above, where Mass 1 represents the 3 blades and Mass 2 represents the combined effects of the nacelle, hub and tower.\n",
    "\n",
    "**Exercise for the reader!** Given the diagram of the 2DOF mass, spring and damper system, derive the equations of motion and give the mass, stiffness and damping matrices.\n",
    "\n",
    "**Answer**.\n",
    "The 2DOF mass-spring-damper system has the following system matrices:\n",
    "\\begin{equation}\n",
    "[M] = \\left[\\begin{array}{cc}m_1 & 0\\\\0 & m_2\\end{array} \\right] \n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "[C] = \\left[\\begin{array}{cc}c_1 & -c_1\\\\-c_1 & c_1+c_2\\end{array} \\right]\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "[K] = \\left[\\begin{array}{cc}k_1 & -k_1\\\\-k_1 & k_1+k_2\\end{array} \\right]\n",
    "\\end{equation}\n",
    "\n",
    "Here is a table of relevant parameter values for Turbie,\n",
    "\n",
    "Symbol |Decription |Value\n",
    "-----|-----|----- \n",
    "$m_b$|Mass of a single blade|41 metric tons\n",
    "$m_n$|Mass of the nacelle|446 metric tons\n",
    "$m_t$|Mass of the tower|628 metric tons\n",
    "$m_h$|Mass of the hub|105 metric tons  \n",
    "$c_1$|Equivalent damping of the blades|4208 N/(m/s)\n",
    "$c_2$|Equivalent damping of the nacelle, hub and tower|12730 N/(m/s)\n",
    "$k_1$|Equivalent stiffness of the blades|1711000 N/m\n",
    "$k_2$|Equivalent stiffness of the nacelle, hub and tower|3278000 N/m\n",
    "$D_{rotor}$|Rotor diameter|180 m\n",
    "$\\rho$|Air density|1.22 kg/m^3\n",
    "\n",
    "and here is the thrust-coefficient look-up table:\n",
    "\n",
    "Wind speed (m/s) | CT (-)\n",
    "------|-----\n",
    "4.0  | 0.923\n",
    "5.0  | 0.919\n",
    "6.0  | 0.904\n",
    "7.0  | 0.858\n",
    "8.0  | 0.814\n",
    "9.0  | 0.814\n",
    "10.0 | 0.814\n",
    "11.0 | 0.814\n",
    "12.0 | 0.577\n",
    "13.0 | 0.419\n",
    "14.0 | 0.323\n",
    "15.0 | 0.259\n",
    "16.0 | 0.211\n",
    "17.0 | 0.175\n",
    "18.0 | 0.148\n",
    "19.0 | 0.126\n",
    "20.0 | 0.109\n",
    "21.0 | 0.095\n",
    "22.0 | 0.084\n",
    "23.0 | 0.074\n",
    "24.0 | 0.066\n",
    "25.0 | 0.059"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69d87383",
   "metadata": {},
   "source": [
    "## Dynamical equations\n",
    "\n",
    "A wind turbine is, as you might expect, forced by the wind. To accurately model aerodynamics, you should include extra time-dependent variables to include phenomena such as dynamic stall, tower shadow, variable turbine speed, etc. For simplicity, we make the following assumptions:  \n",
    "* No dynamic inflow, dynamic stall, or tower shadow;  \n",
    "* Turbie's thrust coefficient is constant for a simulation and can be calculated from the mean wind speed;  \n",
    "* The only aerodynamic forcing is on the blades;  \n",
    "* No spatial variation of turbulence.\n",
    "\n",
    "With these assumptions, the aerodynamic forcing on the blades is given by  \n",
    "\\begin{equation}\n",
    "    f_{aero}(t) = \\frac12\\, \\rho\\, C_T\\, A\\, (u(t) - \\dot{x}_1)\\,|u(t) - \\dot{x}_1|,\n",
    "\\end{equation}\n",
    "where $A$ is the rotor area and $u(t)$ is the wind speed at time $t$. The thrust coefficient $C_T$ is determined from the mean wind speed $U = \\overline{u(t)}$ using the look-up table defined above.\n",
    "\n",
    "The full dynamical equations for Turbie are then given by\n",
    "\\begin{equation}\n",
    "[M]\\ddot{\\overline{x}}(t) + [C]\\dot{\\overline{x}}(t) + [K]\\overline{x}(t) = \\overline{F}(t)\n",
    "\\end{equation}\n",
    "where $\\overline{x}(t)=[x_1(t), x_2(t)]^T$ is the state vector and the forcing vector is given by  \n",
    "\\begin{equation}\n",
    "\\overline{F}(t) = \\left[ \\begin{array}{c} f_{aero}(t)  \\\\ 0\\end{array} \\right].\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8232c79b",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
