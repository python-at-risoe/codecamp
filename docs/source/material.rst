.. _material:

Material
==========

Slides
------------------------

August 2022 (Python and Matlab):  

   * Day 1 :download:`download pdf <../../material/slides/2022b/CodeCamp_Aug2022_Day1.pdf>` 
   * Day 2 :download:`download pdf <../../material/slides/2022b/CodeCamp_Aug2022_Day2.pdf>`  
   * Day 3 :download:`download pdf <../../material/slides/2022b/CodeCamp_Aug2022_Day3.pdf>`  


Cheatsheets
------------------------

A collection of quick references from Matplotlib and Python
for Data Science:  

  * Jupyter notebook cheatsheet :download:`download pdf <../../material/cheatsheets/jupyter_notebook_cheatsheet.pdf>` 
  * Numpy cheatsheet :download:`download pdf <../../material/cheatsheets/numpy_cheatsheet.pdf>` 
  * Matplotlib cheatsheet :download:`download pdf <../../material/cheatsheets/matplotlib_cheatsheet.pdf>` 
  * Numpy cheatsheet :download:`download pdf <../../material/cheatsheets/numpy_cheatsheets.pdf>` 
  * Basic pandas cheatsheet :download:`download pdf <../../material/cheatsheets/basic_pandas_cheatsheet.pdf>` 
  * Intermediate pandas cheatsheet :download:`download pdf <../../material/cheatsheets/intermediate_pandas_cheatsheet.pdf>` 

A cheatsheet for Matlab plotting by peijin94 on GitHub:  

  * Matlab plotting :download:`download png <../../material/cheatsheets/MATLAB_plot_cheatsheet.png>` 


Tutorials and exercises
------------------------

The following links are a series of Jupyter notebooks that allow
you to practice basic skills with Jupyter notebooks and common Python
packages used with scientific programming.

Google how to run a Jupyter notebook if you've never used one before. :)

   * Introduction to Jupyter notebooks :download:`download notebook <../../material/jupyter_notebooks/introduction_to_jupyter_notebooks.ipynb>`  
   * NumPy tutorial :download:`download notebook <../../material/jupyter_notebooks/2a-tutorial_numpy_array_basics.ipynb>`  
   * NumPy exercise :download:`download notebook <../../material/jupyter_notebooks/2b-exercise_numpy.ipynb>`  
   * Matplotlib tutorial :download:`download notebook <../../material/jupyter_notebooks/3a-tutorial_matplotlib_basics.ipynb>`  
   * Matplotlib exercise :download:`download notebook <../../material/jupyter_notebooks/3b-exercise_matplotlib.ipynb>`  
   * Pandas tutorial :download:`download notebook <../../material/jupyter_notebooks/4a-tutorial_pandas.ipynb>`  
   * Pandas exercise :download:`download notebook <../../material/jupyter_notebooks/4b-exercise_pandas.ipynb>`  


Slides from previous editions
------------------------------

January 2022 (Python only):  

   * Day 1 :download:`download pdf <../../material/slides/2022/CodeCamp_Jan2022_Day1.pdf>`  
   * Day 2 :download:`download pdf <../../material/slides/2022/CodeCamp_Jan2022_Day2.pdf>`  
   * Day 3 :download:`download pdf <../../material/slides/2022/CodeCamp_Jan2022_Day3.pdf>`  
   * Day 4 :download:`download pdf <../../material/slides/2022/CodeCamp_Jan2022_Day4.pdf>`  


August 2021 (Python and Matlab):  
 
   * Day 1 :download:`download pdf <../../material/slides/2021/CodeCamp_Day1.pdf>`  
   * Day 2 :download:`download pdf <../../material/slides/2021/CodeCamp_Day2.pdf>` 
   * Day 3 :download:`download pdf <../../material/slides/2021/CodeCamp_Day3.pdf>` 
   * Day 4 :download:`download pdf <../../material/slides/2021/CodeCamp_Day4.pdf>`  