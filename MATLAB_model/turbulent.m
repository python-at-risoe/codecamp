function V = turbulent(V10,TI,l,fixseed)

global t f

% Computes time series of turbulent wind speed given the 10-min averaged 
% wind speed, the turbulence intensity and length scale. Based on the
% Kaimal spectrum

% Frequency resolution [Hz]
df = f(2)-f(1);

% Kaimal spectrum [m2/s]
Skai = 4*TI^2*V10*l./((1+6*f*l./V10).^(5/3));

% Amplitudes [m/s]
bi = sqrt(2.*Skai*df);

% Random phases [rad]
if fixseed, rng(1), end
epsiloni = 2*pi*rand(1,length(f));

% Fourier coefficients
Vhat = bi.*exp(1i*epsiloni);

% Dynamic part
Vdyn = freq2time(f,t,Vhat);

% Time series of wind speed [m/s]
%V = V10 + TI*V10/std(Vdyn)*Vdyn; 
V = V10 + Vdyn; 

return