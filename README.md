# CodeCamp

Spend less time coding and more time learning.

A week-long workshop for DTU Wind Energy students. 

## Website

Please [click here](https://python-at-risoe.pages.windenergy.dtu.dk/codecamp/)
to read more about CodeCamp.