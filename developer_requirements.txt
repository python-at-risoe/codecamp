# requirements for PyConTurb developers
jupyter  # to run notebooks
pillow==8.3.1  # 8.3.2 broke my pipeline, needed for matplotlib
matplotlib  # plotting in notebooks
nbsphinx  # notebook linking in documentation
sphinx  # building documentation
sphinx_rtd_theme  # theme for documentation